CC=g++
CF= -c -Wall -g -O0
LF= -o
SOURCES=$(wildcard *.cpp)
OBJECTS=$(SOURCES:.cpp=.o)
PROG=tag

$(PROG): $(OBJECTS)
	$(CC) $(OBJECTS) $(LF) $(PROG)

$(OBJECTS): $(SOURCES)
	$(CC) $(CF) $(SOURCES)

clean:
	rm *.o tag

start:
	./$(PROG)
