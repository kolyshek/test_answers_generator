#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <random>
#include <algorithm>
#include <sstream>
#include <chrono>

using form_t = std::map<int, std::vector<char>>;
form_t pattern = { {0, {'2', '-'}},
					{1, {'4', '-'}},
					{2, {'5', '+'}},
					{3, {'2', '-'}},
					{4, {'4', '+'}},
					{5, {'4', '-'}},
					{6, {'4', '-'}},
					{7, {'4', '+'}},
					{8, {'4', '+'}},
					{9, {'2', '-'}},
					{10, {'4', '+'}},
					{11, {'5', '+'}},
					{12, {'4', '+'}},
					{13, {'2', '-'}},
					{14, {'2', '-'}}};

form_t generateTest(form_t p)
{
	form_t result;
	for (size_t count = 0; count < 15; ++count)
	{
		std::vector<char> variants;
		std::stringstream ss;
		ss << p[count][0];
		size_t variantsMax;
		ss >> variantsMax;
		for (size_t variantsCount = 0; variantsCount < variantsMax; ++variantsCount)
		{
			variants.push_back(static_cast<char>(97 + variantsCount));
		}
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::shuffle(variants.begin(), variants.end(), std::default_random_engine(seed));
		if (p[count][1] == '+')
		{
			size_t randCount = 1 + std::rand() % variants.size();
			for (size_t answerCount = 0; answerCount < randCount; ++answerCount)
			{
				result[count + 1].push_back(variants[answerCount]);
			}
			std::sort(result[count + 1].begin(), result[count + 1].end());
		}
		else
		{
			result[count + 1].push_back(variants[0]);
		}
	}
	return result;
}

int main()
{
	std::srand((unsigned) time(NULL));
	for (size_t count = 0; count < 30; ++count)
	{
		form_t result = generateTest(pattern);
		for (auto f: result)
		{
			std::cout << f.first << ": ";
			for (auto s: f.second)
			{
				std::cout << s << " ";
			}
			std::cout << "\n";
		}
		std::cout << "~~~~~~~~~~~~~~~~" << "\n";
	}
	return 0;
}
